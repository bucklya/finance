package com.company;

import com.company.data.Generator;
import com.company.inter.Credit;
import com.company.model.Organization;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class RunnerCredit {

    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму-кредита которою вы бы хотели получить: ");
        int inputMoney = Integer.parseInt(scan.nextLine());

        List<Credit> creditList = new ArrayList<>();
        for (Organization organization : organizations) {
            if (organization instanceof Credit) {
                creditList.add((Credit) organization);
            }
        }

        Collections.sort(creditList, (creditList1, creditList2) -> Float.compare(creditList1.getDebtPercent(), creditList2.getDebtPercent()));

        System.out.println("Организации будут отсортированы в порядке возрастания годового процента.");

        float result;
        if (inputMoney > 0) {
            for (Credit credit : creditList) {
                Organization org = (Organization) credit;
                result = credit.crediting(inputMoney);
                if (result != 0) {
                    org.showInfo();
                    System.out.println(String.format("Выплата по кредиту на сумму %d через год будет состовлять = %.2f", inputMoney, result));
                } else {
                    System.out.print("В данной организации операция невозможна: ");
                    org.showInfo();
                }
                System.out.println("-------------------------------------------------------------------------------");
            }
        }
    }
}
