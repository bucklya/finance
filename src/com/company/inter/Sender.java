package com.company.inter;

public interface Sender {
    float sending(int money);
    float getTransferPercentage();
}
