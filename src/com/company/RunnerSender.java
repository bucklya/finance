package com.company;

import com.company.data.Generator;
import com.company.model.Organization;
import com.company.inter.Sender;
import org.w3c.dom.ls.LSInput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class RunnerSender {
    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму для проведения перевода: ");
        int inputMoney = Integer.parseInt(scan.nextLine());

        List<Sender> senderList = new ArrayList<>();
        for (Organization organization : organizations) {
            if (organization instanceof Sender){
                senderList.add((Sender) organization);
            }
        }

        System.out.println("Организации будут отсортированы в порядке убывания процента перевода.");

        Collections.sort(senderList, (senderList1, senderList2) -> Float.compare(senderList1.getTransferPercentage(),senderList2.getTransferPercentage()));

        float result;
        if (inputMoney > 0) {
            for (Sender sender : senderList) {
                Organization org = (Organization) sender;
                result = sender.sending(inputMoney);
                org.showInfo();
                System.out.println(String.format("Для перевода суммы: %d затраты вместе с комиссией будут равны %.2f", inputMoney, result));
                System.out.println("-------------------------------------------------------------------------------");
            }
        } else {
            System.out.println("Операция невозможна");
        }

//        float lowerBetter = Float.MAX_VALUE;
//        int check = 0;
//        for (int i = 0; i < organizations.size(); i++) {
//            if (organizations.get(i) instanceof Sender){
//                Sender sender = (Sender) organizations.get(i);
//                float checkSum = sender.sending(inputMoney);
//                if (checkSum<lowerBetter){
//                    lowerBetter = checkSum;
//                    check = i;
//                }
//            }
//        }
//        System.out.println("Лучшей организацией для предоставления операции денежнего перевода является: ");
//        organizations.get(check).showInfo();
//        System.out.println(String.format("Для перевода суммы: %d затраты вместе с комиссией будут равны %.2f", inputMoney, lowerBetter));
    }
}
