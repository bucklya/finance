package com.company.model;

import com.company.inter.Sender;

public class TransferService extends Organization implements Sender {
    private float transferPercentage;

    public TransferService(String name, String address, float transferPercentage) {
        super(name, address);
        this.transferPercentage = transferPercentage;
    }

    @Override
    public float sending(int money) {
        return money * (1 + transferPercentage);
    }

    @Override
    public float getTransferPercentage() {
        return transferPercentage;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s | Адрес: %s | Процент перевода: %.0f", super.name, super.address, transferPercentage * 100));
    }
}
