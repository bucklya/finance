package com.company.model;

import com.company.inter.Credit;
import com.company.inter.CurrencyExchange;
import com.company.inter.Deposit;
import com.company.inter.Sender;

import java.util.Map;

public class Bank extends Organization implements CurrencyExchange, Credit, Deposit, Sender {
    private static final String ACTION = "продать";
    private int yearLicense;
    private Map<String, Currency> rate;
    private int commissionExchange = 15;
    private int limitExchange = 150000;
    private int debtLimit = 200000;
    private float percentCredit = 0.25f;
    private float percentDeposit = 0.11f;
    private float transferPercentage = 0.01f;

    public Bank(String name, String address, int yearLicense, Map<String, Currency> rate) {
        super(name, address);
        this.yearLicense = yearLicense;
        this.rate = rate;
    }

    @Override
    public boolean isSell(String inputChoose) {
        return ACTION.toLowerCase().contains(inputChoose.toLowerCase());
    }

    @Override
    public float sell(int money, String inputNameCurr) {
        Currency curr = rate.get(inputNameCurr.toLowerCase());
        float cash = money * curr.getSell();
        if (hasMoreThanLimit(cash, limitExchange)) {
            return (cash - commissionExchange);
        }
        return 0;
    }

    @Override
    public float buy(int money, String inputNameCurr) {
        Currency curr = rate.get(inputNameCurr.toLowerCase());
        if (hasMoreThanLimit(money, limitExchange)) {
            return (money - commissionExchange) / curr.getBuy();
        }
        return 0;
    }

    @Override
    public float getCommissionExchange() {
        return commissionExchange;
    }

    @Override
    public float crediting(int money) {
        if (hasMoreThanLimit(money, debtLimit)) {
            return money * (1 + percentCredit);
        }
        return 0;
    }

    @Override
    public float getDebtPercent() {
        return percentCredit;
    }

    @Override
    public float depositing(int money) {
        return money * (1 + percentDeposit);
    }

    @Override
    public float getPercentDeposit() {
        return percentDeposit;
    }

    @Override
    public float sending(int money) {
        return money * (1 + transferPercentage);
    }

    @Override
    public float getTransferPercentage() {
        return transferPercentage;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s | Адрес: %s | Год получения лицензии: %d", super.name, super.address, yearLicense));
        System.out.println("Банк предоставляет услуги: ");
        System.out.println(String.format("Курс обмена гривну на USD, EUR и RUB м наоборот, с минимальной комиссией = %d гривен", commissionExchange));
        System.out.println(String.format("Выдача кредитов на сумму не более %d гривен, под приятный процент годовых = %.0f", debtLimit, percentCredit * 100));
        System.out.println(String.format("Оформление годового депозита с хорошим процентом = %.0f", percentDeposit * 100));
        System.out.println(String.format("Перевод денежных средств с малым процентом = %.0f", transferPercentage * 100));
    }
}
