package com.company.inter;

public interface Deposit {
    float depositing(int money);
    float getPercentDeposit();
}
