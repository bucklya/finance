package com.company.data;

import com.company.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class Generator {
    private Generator() {
    }

    public static List<Organization> generate() {
        List<Organization> organizations = new ArrayList<>();
        {
            Map<String, Currency> rate = new HashMap<>();
            rate.put("usd", new Currency( 27f, 27.4f));
            rate.put("eur", new Currency( 29.25f, 29.95f));
            rate.put("rub", new Currency( 0.325f, 0.375f));
            organizations.add(new Bank("БанкБольшихДенег", "ул. Пушкина 32", 2007, rate));
        }

        {
            Map<String, Currency> rate = new HashMap<>();
            rate.put("usd", new Currency( 26.9f, 27.5f));
            rate.put("eur", new Currency( 29.1f, 30f));
            organizations.add(new Exchanger("Обменка Туда-Сюда", "ул. Гагарина 28", rate));
        }
            organizations.add(new CreditOrg("Ломбард 24/7", "ул. Культуры 16", 50000, 0.4f));

            organizations.add(new CreditOrg("Cafe Banker", "ул. Дарвина 3", 4000, 2f));

            organizations.add(new CreditOrg("Credit Union", "ул. Дмитриевская 26", 100000, 0.2f));

            organizations.add(new Fond("Фонд инвестиций в ваше будущее", "ул. Попова 99/2", 1998, 0.12f));

            organizations.add(new TransferService("Голубиная почта", "пр. Печкина 14",0.05f));

            organizations.add(new TransferService("Сервис онлайн переводов <Пошлем куда надо>", "ул. Вишневая 37",0.03f));

        return organizations;
    }
}