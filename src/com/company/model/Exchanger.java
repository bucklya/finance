package com.company.model;

import com.company.inter.CurrencyExchange;

import java.util.Map;

public class Exchanger extends Organization implements CurrencyExchange {
    private static final String ACTION = "продать";
    private Map<String, Currency> rate;

    public Exchanger(String name, String address, Map<String, Currency> rate) {
        super(name, address);
        this.rate = rate;
    }

    @Override
    public boolean isSell(String inputChoose) {
        return ACTION.toLowerCase().contains(inputChoose.toLowerCase());
    }

    @Override
    public float sell(int money, String inputNameCurr) {
        Currency curr = rate.get(inputNameCurr.toLowerCase());
        if (curr == null) {
            return 0;
        } else {
            return (money * curr.getSell());
        }
    }

    @Override
    public float buy(int money, String inputNameCurr) {
        Currency curr = rate.get(inputNameCurr.toLowerCase());
        if (curr == null) {
            return 0;
        } else {
            return money / curr.getBuy();
        }
    }

    @Override
    public float getCommissionExchange() {
        return 0;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s | Адрес: %s | Без комиссии! ", super.name, super.address));
    }
}
