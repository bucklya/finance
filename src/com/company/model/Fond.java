package com.company.model;

import com.company.inter.Deposit;

public class Fond extends Organization implements Deposit {
    private int yearOfFoundation;
    private float percentDeposit;

    public Fond(String name, String address, int yearOfFoundation, float percent) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
        this.percentDeposit = percent;
    }

    @Override
    public float depositing(int money) {
        return money * (1 + percentDeposit);
    }

    @Override
    public float getPercentDeposit() {
        return percentDeposit;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s | Адрес: %s | Год основания: %d | Процент годового депозита: %.0f", super.name, super.address, yearOfFoundation, percentDeposit * 100));
    }
}
