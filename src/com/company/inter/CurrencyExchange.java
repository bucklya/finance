package com.company.inter;

public interface CurrencyExchange {
    boolean isSell(String inputChoose);
    float sell(int money, String inputNameCurr);
    float buy(int money, String inputNameCurr);
    float getCommissionExchange();
}
