package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите операцию которую вы хотите произвести (Обмен валюты, Депозит, Кредит, Перевод)");
        Scanner scan = new Scanner(System.in);
        String inputChoose = scan.nextLine();

        if ("обмен валюты".contains(inputChoose.toLowerCase())) {
            new RunnerCurrExch().run();
        } else if ("кредит".contains(inputChoose.toLowerCase())) {
            new RunnerCredit().run();
        } else if ("депозит".contains(inputChoose.toLowerCase())) {
            new RunnerDeposit().run();
        } else if ("перевод".contains(inputChoose.toLowerCase())) {
            new RunnerSender().run();
        } else {
            System.out.println("Неверно введена операция");
        }
    }
}
