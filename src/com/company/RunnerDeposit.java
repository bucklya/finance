package com.company;

import com.company.data.Generator;
import com.company.inter.Deposit;
import com.company.model.Organization;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class RunnerDeposit {
    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму которую вы хотите положить на годовой депозит: ");
        int inputMoney = Integer.parseInt(scan.nextLine());

        List<Deposit> depositList = new ArrayList<>();
        for (Organization organization : organizations) {
            if (organization instanceof Deposit) {
                depositList.add((Deposit) organization);
            }
        }

        Collections.sort(depositList, (depositList1, depositList2) -> Float.compare(depositList2.getPercentDeposit(), depositList1.getPercentDeposit()));

        System.out.println("Организации будут отсортированы в порядке убывания годового процента.");

        float result;
        if (inputMoney > 0) {
            for (Deposit deposit : depositList) {
                Organization org = (Organization) deposit;
                result = deposit.depositing(inputMoney);
                org.showInfo();
                System.out.println(String.format("Сумма на депозите через год будет состовлять = %.2f", result));
                System.out.println("-------------------------------------------------------------------------------");
            }
        } else {
            System.out.println("Операция невозможна");
        }
    }
}
