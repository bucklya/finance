package com.company.model;

public class Currency {
    private float buy;
    private float sell;

    public Currency(float buy, float sell) {
        this.buy = buy;
        this.sell = sell;
    }

    public float getBuy() {
        return buy;
    }

    public float getSell() {
        return sell;
    }
}
