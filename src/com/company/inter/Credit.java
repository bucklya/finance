package com.company.inter;

public interface Credit {
    float crediting(int money);
    float getDebtPercent();
}
