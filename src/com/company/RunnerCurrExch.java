package com.company;

import com.company.data.Generator;
import com.company.inter.CurrencyExchange;
import com.company.model.Currency;
import com.company.model.Organization;

import java.util.*;

public class RunnerCurrExch {
    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("С какой валютой вы хотите провести операцию (usd, eur, rub): ");
        String inputNameCurr = scan.nextLine();

        System.out.println("Продать или купить?: ");
        String inputChoose = scan.nextLine();

        System.out.println("Введите сумму: ");
        int inputMoney = Integer.parseInt(scan.nextLine());

        List<CurrencyExchange> currencyExchangeList = new ArrayList<>();
        for (Organization organization : organizations) {
            if (organization instanceof CurrencyExchange) {
                currencyExchangeList.add((CurrencyExchange) organization);
            }
        }

        Collections.sort(currencyExchangeList, (currencyExchangeList1, currencyExchangeList2) -> Float.compare(currencyExchangeList1.getCommissionExchange(), currencyExchangeList2.getCommissionExchange()));

        System.out.println("Организации будут отсортированы в порядке возрастания комиссии.");

        float result;
        if (inputMoney > 0) {
            for (CurrencyExchange currencyExchange : currencyExchangeList) {
                Organization org = (Organization) currencyExchange;
                if (currencyExchange.isSell(inputChoose)) {
                    result = currencyExchange.sell(inputMoney, inputNameCurr);
                } else {
                    result = currencyExchange.buy(inputMoney, inputNameCurr);
                }
                if (result != 0) {
                    org.showInfo();
                    System.out.println(String.format("Где результат вашей операции: %.2f", result));
                }
                System.out.println("-------------------------------------------------------------------------------");
            }
        } else {
            System.out.println("Операция невозможна");
        }
    }
}
