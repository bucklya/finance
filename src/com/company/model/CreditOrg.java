package com.company.model;

import com.company.inter.Credit;

public class CreditOrg extends Organization implements Credit{
    private int debtLimit;
    private float percent;

    public CreditOrg(String name, String address, int debtLimit, float percent) {
        super(name, address);
        this.debtLimit = debtLimit;
        this.percent = percent;
    }

    @Override
    public float crediting(int money) {
        if (hasMoreThanLimit(money, debtLimit)) {
            return money * (1 + percent);
        }
        return 0;
    }

    @Override
    public float getDebtPercent() {
        return percent;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s | Адрес: %s | Максимальная сумма кредита %d | Процент годовых: %.0f", super.name, super.address, debtLimit, percent * 100));
    }
}
